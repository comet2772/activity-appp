
import { useState, useMemo } from 'react';
import constants from '../../constants/index';

interface ServerState{
  isLoading?: boolean,
  isError?: boolean,
  isSuccess?: boolean
  error?: Error,
  data?: any,
  sCount?: number,
  eCount?: number
}

export const defaultServerState = {
  isError: false,
  isLoading: false,
  isSuccess: false,
  data: {},
  sCount: 0,
  eCount: 0
}

export function useActivityFetch(){

  const [serverStates, setStates] = useState<ServerState>({});

  function fetchActivity(url: string, options?: any){
    setStates({isLoading: true});
    fetch(url, options)
    .then((res) => {
      if(!res.ok){
        throw new Error('Something went wrong');
      }
      return res.json();
    })
    .then((data: any) => {

      const rActivityLog = localStorage.getItem(constants.DATA_KEY);
      let activityLog =  rActivityLog ? JSON.parse(rActivityLog) : [];
      activityLog.push({...data});
      localStorage.setItem(constants.DATA_KEY, JSON.stringify([...activityLog]));

      setStates({
        isLoading: false,
        isSuccess: true,
        data: {...data},
        sCount: serverStates?.sCount ? serverStates.sCount + 1 : 1
      });
    })
    .catch((e: Error) => setStates({
      isLoading: false,
      isError: true,
      error: e,
      eCount: serverStates?.eCount ? serverStates.eCount + 1 : 1
    }));
  }

  return {...serverStates, setStates, fetchActivity};
}