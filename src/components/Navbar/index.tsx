import React from "react";
import logo from '../../logo.svg';

const Navbar: React.FC = (): JSX.Element => {
  return (
    <nav className="navbar navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand align-items-center d-flex" href="#">
            <img src={logo} alt="" width="30" height="24" className="d-inline-block align-text-top"/>
            Activity Manager
          </a>
        </div>
    </nav>
  )
}

export default Navbar;