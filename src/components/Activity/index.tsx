import React from 'react';
import "./index.scss";
import _ from 'lodash';
import { ActivityProps } from './props.types';
import { IActivity } from './../../common/types/IActivity';

const Activity: React.FC<ActivityProps> = ({activity, isError, isLoading, isSuccess, error}) : JSX.Element => {
  return (
    <div className='activity-wrapper'>

      <div className='activity-header bg-light'>
        <div className="container-fluid">
          <div className='activity-header-title'>Recent Activity</div>
        </div>
      </div>

      <div className='activity-content container'>
        { isLoading 
          ? <div className='activity-placeholder'>Loading...</div>
          : (isError && error) 
            ? <div className='activity-placeholder text-danger'>{error.toString()}</div>
            : (!_.isEmpty(activity))
              ? <div>
                  {activity && Object.keys(activity).map((aKey: any, aIndex) => {
                    let value = activity[aKey as keyof IActivity];
                    return (
                      <div className='row my-3'>
                        <div className='col-6 activity-content-label'>
                          {aKey.toString()}
                        </div>
                        <div className='col-6 activity-content-value'>
                          {value 
                            ? (aKey === 'link' && value)
                                ? <a href={value.toString()} target='_blank'>{value}</a>
                                : value
                            : '-' }
                        </div>
                      </div>
                    );
                  })}
                </div> 
              : <div className='activity-placeholder'>No recent activity!</div>
        }
      </div>
    </div>
  );
}

export default Activity;