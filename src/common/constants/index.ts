const constants = {
  API_URL: 'https://www.boredapi.com/api/activity',
  DATA_KEY: 'activityLog',
}

export default constants;