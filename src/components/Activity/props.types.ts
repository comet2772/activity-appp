import { IActivity } from './../../common/types/IActivity';

export interface ActivityProps{
  activity?: IActivity;
  isLoading?: boolean,
  isError?: boolean,
  isSuccess?: boolean
  error?: Error,
}