import React, { useEffect, useState } from "react";
import { IActivity } from './../../common/types/IActivity';
import constants from './../../common/constants/index';
import EyeIcon from '../../assets/icons/eye.svg';

const ActivityList: React.FC<{count?: any, handleViewActivity: (p: IActivity) => void }> = ({count, handleViewActivity}) : JSX.Element => {

  const [activities, setActivities] = useState<Array<IActivity>>([]);

  /**
   * Fetch data from localStorage API
   */
  function fetchData(){
    let rData = localStorage.getItem(constants.DATA_KEY);
    setActivities(rData ? JSON.parse(rData) : []);
  }

  // fetches data on component mount
  useEffect(() => {
    fetchData();
  }, []);

  // fetches data, after add & reset activity
  useEffect(() => {
      fetchData();
  }, [count]);


  return(
    <div className='container'>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Activity</th>
            <th scope="col">Type</th>
            <th scope="col">Price</th>
            <th scope="col">Accessibility</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {activities && activities.map((activity: IActivity, index: number) => {
            return(
              <tr>
                <th scope="row">{index + 1}</th>
                <td>
                  <div className="col-6 text-truncate">
                    {activity.activity}
                  </div>
                </td>
                
                <td>
                  <div>
                    {activity.type}
                  </div>
                </td>

                <td>
                  <div>
                    {activity.price}
                  </div>
                </td>

                <td>
                  <div>
                    {activity.accessibility}
                  </div>
                </td>

                <td>
                  <div>
                    <img alt="View Activity" onClick={() => handleViewActivity({...activity})} title="View Activity" src={EyeIcon}/>
                  </div>
                </td>
              </tr>
            );
          })} 
        </tbody>
      </table>
      {(!activities || activities.length == 0) &&
        <div className="d-flex flex-column w-100 align-items-center mt-5">
            <h5>No Activity!</h5>
            <p className="text-muted">All your activities will be shown here.</p>
        </div>
      }
    </div>
  );
}

export default ActivityList;