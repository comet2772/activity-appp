import React, { useState } from 'react';
import "./index.scss";
import { defaultServerState, useActivityFetch } from '../../common/hooks/useActivityFetch';
import Activity from './../../components/Activity/index';
import constants from './../../common/constants/index';
import ActivityList from '../../components/ActivityList';
import Navbar from './../../components/Navbar/index';
import _ from 'lodash';

const Activities: React.FC = () : JSX.Element => {

  const { isError, isLoading, isSuccess, error, sCount, data: recentActivity, fetchActivity, setStates} = useActivityFetch();
  const [selectedActivity, setActivity] = useState({});

  function handleAddActivity(){
    fetchActivity(constants.API_URL);
  }

  function handleClearAll(){
    setStates({...defaultServerState});
    localStorage.removeItem(constants.DATA_KEY);
  }

  return (
    <div className='activities-wrapper'>
      <div className='activities-control'>      
        <Navbar/>
        <div className='container mt-3'>
          <div className='d-flex justify-content-between'>
            <button type='button' className="btn btn-primary" onClick={handleAddActivity}>Add Activity</button>
            <button type='button' className="btn btn-warning" onClick={handleClearAll}>Clear All</button>
          </div>
          <div className='mt-5'>
            <ActivityList count={sCount} handleViewActivity={(obj) => setActivity({...obj})}/>
          </div>
        </div>
      </div>
      
      {/* ------ Show recent activity ------ */}
      <div className='activities-recent-history'>
        <Activity
          activity={!_.isEmpty(selectedActivity) ? selectedActivity : recentActivity}
          isLoading={isLoading}
          isSuccess={isSuccess}
          isError={isError}
          error={error}
        />
      </div>
    </div>
  );
}

export default Activities;